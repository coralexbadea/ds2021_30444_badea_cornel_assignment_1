import { TestBed } from '@angular/core/testing';

import { SpringbootInterceptor } from './springboot.interceptor';

describe('SpringbootInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      SpringbootInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: SpringbootInterceptor = TestBed.inject(SpringbootInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
