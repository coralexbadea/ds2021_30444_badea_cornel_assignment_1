import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { API } from './constants';
import 'rxjs/add/operator/map'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private BASE_URL = `${API}/authenticate`;

  constructor(private http: HttpClient,) { }

  login(payload: any){
    let httpHeaders = new HttpHeaders({
      // 'Cache-Control': 'no-cache',
      // 'Access-Control-Allow-Headers': 'Content-Type',
      // 'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Credentials' : 'true',
      'Content-Type' : 'application/json',
      'Accept'       : 'application/json',
      'skip':'true'
    });

    // var fd = new FormData()
    // fd.append('userName', payload.userName)
    // fd.append('password', payload.password)
    // fd.append('Submit', 'Login')

    return this.http.post(this.BASE_URL, payload, {...Option, headers:{skip:"true"},  observe:'response' })
    .pipe(catchError(this.handleError))
    .map(response=>
      {
        return response.body
      }
    ); 
  }

  handleError(error:any){
    if(error.status == 0)
      console.error('Error:',error.error)
    else{
      console.error('Error, backend returned:',error.error);
    }
    return throwError(
      error.error
    )
  }
}