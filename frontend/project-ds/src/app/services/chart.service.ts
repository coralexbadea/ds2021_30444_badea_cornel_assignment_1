import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  private deviceId: number = -1;
  private subject = new Subject<any>();

  private days: number = 1;
  private subject1 = new Subject<any>();


  constructor() { }

  setDeviceId(newId:number){
    this.deviceId = newId
    this.subject.next(this.deviceId)
  }


  getSubjectDeviceId():Observable<any>{
    return this.subject.asObservable()
  }

  setLastDays(days:number){
    this.days = days
    this.subject1.next(this.days)
  }


  getSubjectLastDays():Observable<any>{
    return this.subject1.asObservable()
  }



}
