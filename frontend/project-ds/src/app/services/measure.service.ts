import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Measure } from '../models/Measure';
import { User } from '../models/User';
import { API } from './constants';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {

  private BASE_URL=`${API}/measure`;
  private user:User|null = null
  private httpHeaders = new HttpHeaders({
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
    // 'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials' : 'true',
    'Authorization': `Bearer ${localStorage.getItem("token")}`,
  });

  constructor(private http:HttpClient, private store:Store<any>) {
   }

  getMeasuresOneDay(deviceId: number, date : Date):Observable<any>{
    date.setHours(0)
    return this.http .post<Measure[]>(`${API}`,
    {
      "id":"1",
      "jsonrpc":"2.0",
        "method":"rpcGetMeasuresFromDate",
        "params":{
          "id":deviceId,
          "payload":{
            "timestamp":date.getTime()
          }
        }
    }, {...Option, headers:this.httpHeaders,  observe:'response' });
  }


  getMeasuresLastDays(userId:number, days:number):Observable<any>{
    return this.http .post<Measure[]>(`${API}`,
    {
      "id":"1",
      "jsonrpc":"2.0",
        "method":"rpcGetMeasuresLastDays",
        "params":{
          "id":userId,
          "d":days
        }
      }, 
    {...Option, headers:this.httpHeaders,  observe:'response' });

  }


  getBaseline(userId:number):Observable<any>{
    return this.http .post<Measure[]>(`${API}`,
    {
      "id":"1",
      "jsonrpc":"2.0",
        "method":"rpcGetBaseline",
        "params":{
          "id":userId
        }
      }
      , 
    {...Option, headers:this.httpHeaders,  observe:'response' });

  }


  getIntervals(userId:number, duration:number):Observable<any>{
    return this.http .post<Measure[]>(`${API}`,
    {
      "id":"1",
      "jsonrpc":"2.0",
        "method":"rpcGetStartTimes",
        "params":{
          "id":userId,
          "duration":duration
        }
      }
      , 
    {...Option, headers:this.httpHeaders,  observe:'response' });

  }

  

  
}
