import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { observable } from 'rxjs';
import { Observable } from 'rxjs-compat';
import { User } from '../models/User';
import { API } from './constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private BASE_URL=`${API}/user`;

  private httpHeaders = new HttpHeaders({
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
    // 'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials' : 'true',
    'Authorization': `Bearer ${localStorage.getItem("token")}`,
  });

  constructor(private http:HttpClient) { 
    
  }

  getUsers():Observable<User[]>{
    return (this.http.get<User[]>(`${this.BASE_URL}`, {headers:this.httpHeaders}));
  }

  deleteUser(id:number){
    return this.http.delete(`${this.BASE_URL}/${id}`, {...Option, responseType: 'text', observe:'response' })
  }

  updateUser(user:User){
    return this.http.put(`${this.BASE_URL}/${user.userId}`,user,{...Option, observe:'response' } )
  }
}
