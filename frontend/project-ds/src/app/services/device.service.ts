import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Device } from '../models/Device';
import { Observable, of } from 'rxjs';
import { API } from './constants';
@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  
  private BASE_URL= `${API}/device`;

  private httpHeaders = new HttpHeaders({
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
    // 'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials' : 'true',
    'Authorization': `Bearer ${localStorage.getItem("token")}`,
  });

  constructor(private http:HttpClient) { }

  getDevices(deviceId: number | undefined):Observable<Device[]>{
    return (this.http .get<Device[]>(`${this.BASE_URL}/forUser/${deviceId}`));
  }

  deleteDevice(id:number){
    return this.http.delete(`${this.BASE_URL}/${id}`, {...Option, responseType: 'text', observe:'response' })
  }

  updateDevice(device:Device){
    return this.http.put(`${this.BASE_URL}/${device.deviceId}`,device,{...Option, observe:'response' } )
  }

  addDevice(device:Device){
    return this.http.post(`${this.BASE_URL}`,device,{...Option, observe:'response' } )
  }


}
