import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Store, } from '@ngrx/store';
import { map, tap } from 'rxjs/operators';
import * as fromUserState from 'src/app/store/user/user.selectors';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private router:Router, private store: Store<any>) { 
  
  }

  canActivate(route: ActivatedRouteSnapshot){
    

    return this.store.select(fromUserState.getUserState)
    .pipe(map(result => {
      if(!result || !result.roles?.includes(route.data.role)) {
        this.router.navigate(['/login']);
        return false;
      }
      return true;
    }));


  }

}
