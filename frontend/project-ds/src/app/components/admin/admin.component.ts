import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public users : User[] = [];
  public errorMessage: string = ""
  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(users=>{
      this.users = users;
    })
  }

  deleteUser(id:number){
    console.log(id)
    this.userService.deleteUser(id).subscribe(result=>console.log(result.body))
    this.users = this.users.filter(user=>
      user.userId !== id
    )
  }

  editUser(user: User){
    console.log(user)
    this.userService.updateUser(user).subscribe(
      data => this.errorMessage = "",
      err => this.errorMessage = "Operation Not Permitted"
    )
    this.users = this.users.map((u)=>{
      if(u.userId==user.userId){
        return user
      }
      else{
        return u
      }
    })
  }

}
