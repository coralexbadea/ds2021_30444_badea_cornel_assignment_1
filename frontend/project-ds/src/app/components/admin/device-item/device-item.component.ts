import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons';
import { Device } from 'src/app/models/Device';

@Component({
  selector: 'app-device-item',
  templateUrl: './device-item.component.html',
  styleUrls: ['./device-item.component.scss','../admin.component.scss']
})
export class DeviceItemComponent implements OnInit {
  @Input() device: Device = new Device()
  @Output() onDeleteDevice : EventEmitter<number> = new EventEmitter()
  @Output() onEditDevice : EventEmitter<Device> = new EventEmitter()

  public message: string = ""
  public hideForm: boolean = true;
  faTimes = faTimes
  faEdit = faEdit

  constructor() { }

  ngOnInit(): void {
  }

  onEdit(){
    this.hideForm = !this.hideForm
  }

  onDelete(){
    this.onDeleteDevice.emit(this.device.deviceId)
  }

  onSubmit(){
    if(this.device.maxEnCons && this.device.sensorMaxVal && this.device.maxEnCons > this.device?.sensorMaxVal){
      this.message = "My man Max. En. Cons. of Device must be less than sensor Max. Value"
      return false
    }
    if(this.device.deviceName?.length==0 || this.device.location?.length==0 ||
      this.device.sensorDescription?.length == 0){
        this.message = "Please no empty fields"
        return false
    }
    this.onEditDevice.emit(this.device)
    return true
  }


}
