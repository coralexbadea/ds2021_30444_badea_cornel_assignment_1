import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {faTimes} from "@fortawesome/free-solid-svg-icons"
import {faEdit} from "@fortawesome/free-solid-svg-icons"
import {faLaptopHouse} from "@fortawesome/free-solid-svg-icons"
import { Router } from '@angular/router';

import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-client-item',
  templateUrl: './client-item.component.html',
  styleUrls: ['./client-item.component.scss', '../admin.component.scss']
})
export class ClientItemComponent implements OnInit {

  @Input() user: User = new User()
  @Output() onDeleteUser : EventEmitter<number> = new EventEmitter()
  @Output() onEditUser : EventEmitter<User> = new EventEmitter()

  public bdate: string = this.user.bdate ? this.user.bdate.split('T')[0] : ""
  public hideForm: boolean = true;
  faTimes = faTimes
  faEdit = faEdit
  faLaptopHouse = faLaptopHouse
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onEdit(){
    this.hideForm = !this.hideForm
  }

  onDelete(){
    this.onDeleteUser.emit(this.user.userId)
  }

  onSubmit(){
    this.onEditUser.emit(this.user)
  }

  getDevices(){
    console.log("devices")
    this.router.navigate([`/admin/${this.user.userId}`])
  }


}
