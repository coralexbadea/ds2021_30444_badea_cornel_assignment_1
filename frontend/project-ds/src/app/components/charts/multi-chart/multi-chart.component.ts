import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ChartOptions, ChartType,  ChartDataSets} from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { Measure } from 'src/app/models/Measure';
import { User } from 'src/app/models/User';
import { ChartService } from 'src/app/services/chart.service';
import { MeasureService } from 'src/app/services/measure.service';
import * as fromUserState from 'src/app/store/user/user.selectors';


@Component({
  selector: 'app-multi-chart',
  templateUrl: './multi-chart.component.html',
  styleUrls: ['./multi-chart.component.scss']
})
export class MultiChartComponent implements OnInit {
  private days:number = 1;
  private user:User|null = null;

  private valuesOneDay : number[] =[];
  public lineChartData: ChartDataSets[] = [];
  
  public lineChartLabels: Label[] = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"];
    
  public lineChartOptions = {
    responsive: true,
  };
     
  public lineChartLegend = true;
  public lineChartType : ChartType = 'line'
  public lineChartPlugins = [];
    

  constructor(private measureService : MeasureService,
    private chartService : ChartService,
    private store:Store<any>) {}
    
   
  ngOnInit() {

    this.store.select(fromUserState.getUserState).subscribe(user => {
      this.user = user;
    })

    this.chartService.getSubjectLastDays().subscribe(days => {
      this.days = days
      this.getMeasureSubscription();
    })

  }

  getMeasureSubscription(){
    var userId = (this.user?.userId)? this.user?.userId : 5
    this.measureService.getMeasuresLastDays(userId, this.days).subscribe(response => {
      console.log(response)
      var measuresPerDay = response.body.result
      this.lineChartData = []
        Object.keys(measuresPerDay).map(date => {
          var measuresOneDay = measuresPerDay[date]
          if(measuresOneDay != null){
            this.valuesOneDay = this.getValuesFromMeasures(measuresOneDay)
            this.lineChartData.push({ data:  this.valuesOneDay, label: date.split("T")[0] })
            console.log(this.valuesOneDay)
          }
          
        })

      }
    )
  }

  getValuesFromMeasures(measures : Measure[]) : number[]{
    const average = (arr: number[]) => arr.reduce((a,b) => a + b, 0) / arr.length;
    var result : number[] = [];
    
    for(let i=0; i <= 24; i++){
      var avgHour = average(measures.filter((m) => {
        if(m.hour)
          return (m.hour >= i && m.hour < (i+1))
        else
          return false;
      }).map((m)=>m.enCons?m.enCons:0))
      result.push(avgHour?avgHour:0)
    }
   
    return result
  }

}
