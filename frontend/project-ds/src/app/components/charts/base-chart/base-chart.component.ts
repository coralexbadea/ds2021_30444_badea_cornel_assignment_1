import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ChartOptions, ChartType,  ChartDataSets} from 'chart.js';
import { map } from 'jquery';
import { Color, Label } from 'ng2-charts';
import { Device } from 'src/app/models/Device';
import { Measure } from 'src/app/models/Measure';
import { User } from 'src/app/models/User';
import { ChartService } from 'src/app/services/chart.service';
import { DeviceService } from 'src/app/services/device.service';
import { MeasureService } from 'src/app/services/measure.service';
import * as fromUserState from 'src/app/store/user/user.selectors';


@Component({
  selector: 'app-base-chart',
  templateUrl: './base-chart.component.html',
  styleUrls: ['./base-chart.component.scss']
})
export class BaseChartComponent implements OnInit {

  public devices : Device[] = [];
  public stringSelectedDevices = "Select Device"
  public selectedDevice : number = -1;

  private user:User|null = null;


  private baselineMeasures : number[] =[];

  public lineChartData: ChartDataSets[] = [];
  durationProgram: string = "0"
  durationError:string = ""
  public intervals : string[] = []

  public lineChartLabels: Label[] = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"];
    
  public lineChartOptions = {
    responsive: true,
  };
     
  public lineChartLegend = true;
  public lineChartType : ChartType = 'line'
  public lineChartPlugins = [];
    

  constructor(private measureService : MeasureService,
    private chartService : ChartService, private deviceService:DeviceService,
    private store:Store<any>) {}
    
   
  ngOnInit() {

    this.store.select(fromUserState.getUserState).subscribe(user => {
      this.user = user;
      this.deviceService.getDevices(this.user?.userId).subscribe(devices =>
        {
          if(devices.length==0){this.stringSelectedDevices="No devices found"}
          this.devices=devices
        })
      this.getMeasureSubscription()
    })
  }

  getMeasureSubscription(){
    var userId = (this.user?.userId)? this.user?.userId : 5
    this.measureService.getBaseline(userId).subscribe(response => {
      console.log(response)
      var measuresPerHour = response.body.result
      this.lineChartData = []
      this.baselineMeasures = Object.keys(measuresPerHour).map(hour => {
          var measuresOneHour = measuresPerHour[hour]
          const average = (arr: number[]) => arr.reduce((a,b) => a + b, 0) / arr.length;
          return average(measuresOneHour.map((m:Measure)=>m.enCons?m.enCons:0))  
        })
      this.baselineMeasures = this.baselineMeasures.map(m=>isNaN(m)?.0:m)
      this.baselineMeasures[0] = .0
      this.lineChartData = [{ data:  this.baselineMeasures, label: "baseline KWh" }]


      }
    )
  }


  onSubmitProgram(){
    if(this.selectedDevice == -1){
      this.durationError = "Please, select device"
    }
    else if(+this.durationProgram < 1 || +this.durationProgram > 24){
      this.durationError = "Please, the value is in the interval 1-24"
    }
    else{
      this.durationError = ""
      var userId = (this.user?.userId)? this.user?.userId : 5
      var newBaseline = new Array(24).fill(0);

      this.measureService.getIntervals(userId, +this.durationProgram).subscribe(response=>{
          this.intervals = response.body.result
          
          var device = this.devices.find(d => d.deviceId===this.selectedDevice)
          console.log(device)
          var i1:number = +(this.intervals[0].split("-")[0])
          var i2 :number= +(this.intervals[0].split("-")[1])

          for(var i:number = 0; i < 24; i++){
            newBaseline[i] += this.baselineMeasures[i] ;
          }

          for(var i:number = i1; i < i2; i++){
            newBaseline[i] += device?.maxEnCons?device?.maxEnCons:0 ;
          }

          this.lineChartData = [{ data:  newBaseline, label: "baseline KWh" }]

          
        })
      
    }
  }


}




      // var sumIntervalMap :{[key: string]: number} = {}
      
      // var duration = +this.durationProgram

      // console.log(this.baselineMeasures)
      // for(var i:number=0; i<this.baselineMeasures.length-duration; i++){
      //   var sum = 0;
      //   for(var j:number=0; j<duration; j++){
      //     sum += this.baselineMeasures[i+j]
      //   }
      //   sumIntervalMap[`${i}-${i+j}`] = sum
      // }

      // this.intervals = Object.values(Object.entries(sumIntervalMap).sort((a,b)=>b[1]-a[1]).map(el=>el[0]))

      // console.log(this.intervals)
