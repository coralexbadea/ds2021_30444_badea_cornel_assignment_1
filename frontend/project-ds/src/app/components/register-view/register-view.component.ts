
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';
import { User } from 'src/app/models/User';
import { Router } from '@angular/router';



@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.scss']
})
export class RegisterViewComponent implements OnInit {

  signupAttempt = false;
  user: User = new User();
  registerError : string | undefined
  errorJson : any = {}
  data :any
  loading = false
  emailPattern =   "^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$" ;

  registerForm = this.fb.group(
    {
        username: ['',[Validators.required,Validators.minLength(4),Validators.maxLength(25)]],
        password: ['',[Validators.required,Validators.minLength(7),Validators.maxLength(30)]],
        name: ['',[Validators.required,Validators.minLength(4),Validators.maxLength(25)]],
        address: ['',[Validators.required,Validators.minLength(4),Validators.maxLength(30)]],
        bdate:['',[Validators.required,Validators.pattern(this.emailPattern)]],
    }
  );

  constructor(
    private fb:FormBuilder,
    private registerService : RegisterService,
    private router:Router){
  }
  ngOnInit() {
    
  }

  navigateToLogin(){
    this.router.navigate(['/login'])
  }

  bdateInputErrors(){
    return this.validateBdate() || this.emptyBdate()
  }


  usernameInputErrors(){
    return this.validateUsername() || this.emptyUsername()
  }

  passwordInputErrors(){
    return this.validatePassword() || this.emptyPassword()
  }

  validateBdate(){
    return this.registerForm.controls['bdate'].hasError('pattern')
  }

  validateUsername(){
    return this.registerForm.controls['username'].hasError('minlength') || this.registerForm.controls['username'].hasError('maxlength')
  }

  validatePassword(){
     return this.registerForm.controls['password'].hasError('minlength') || this.registerForm.controls['password'].hasError('maxlength')
  }

  emptyUsername(){
    return this.registerForm.controls['username'].hasError('required')
  }

  emptyPassword(){
    return this.registerForm.controls['password'].hasError('required')
  }

  emptyBdate(){
    return this.registerForm.controls['bdate'].hasError('required')
  }

  resetErrorMessage(){
    this.registerError = undefined
  }


  async buttonPressed(){
    this.signupAttempt = true
    this.loading = true

    if(this.bdateInputErrors()  || this.usernameInputErrors() || this.passwordInputErrors()){
      return
    }

    var date =  new Date(this.registerForm.value.bdate)
    this.user.userName=this.registerForm.value.username
    this.user.password=this.registerForm.value.password
    this.user.address = this.registerForm.value.address 
    this.user.name = this.registerForm.value.name 
    this.user.bdate = date.toISOString().split('T')[0]
  

    await this.registerService.register(this.user).toPromise()
                    .catch( 
                      err => this.errorJson = JSON.parse(err)
                    )
      
    if(this.errorJson.status == 409){
      this.registerError = "The username already exists"
    }
    else{
      this.router.navigate(['/home']);
    }

    
  }

}
