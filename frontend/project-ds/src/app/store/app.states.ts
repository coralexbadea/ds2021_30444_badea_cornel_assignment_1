import { ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { reducer, UserState } from './user/user.reducers';


export const FEATURE_NAME = "officePlanner";
export const selectOfficePlanner = createFeatureSelector<AppState, OfficePlannerState>(FEATURE_NAME);

export interface OfficePlannerState{
    currentUser: UserState;
}

export interface AppState{
    officePlanner:OfficePlannerState
}

export const reducers:ActionReducerMap<OfficePlannerState, any> = {
    currentUser: reducer,
};


