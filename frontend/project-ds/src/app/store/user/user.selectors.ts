import { createSelector } from "@ngrx/store";
import { OfficePlannerState, selectOfficePlanner} from "../app.states";
import { JwtHelperService } from '@auth0/angular-jwt';

export const selectCurrentUser = createSelector(
    selectOfficePlanner,
    (state: OfficePlannerState) => {
      return state.currentUser;
    }
  );

export const getUserState = createSelector(
    selectCurrentUser,
    state => {
      return state.user;
    }
  );

export const getLoginState = createSelector(
  selectCurrentUser,
  state => {
    const jwtHelper:JwtHelperService = new JwtHelperService()
    return state.isLoggedin && !jwtHelper.isTokenExpired(state.user?.token)
  }
);

export const getLoginErrorMessage = createSelector(
  selectCurrentUser,
  state => {
    return state.errorMessage;
  }
);
