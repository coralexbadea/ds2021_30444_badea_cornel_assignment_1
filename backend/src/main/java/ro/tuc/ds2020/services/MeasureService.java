package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.CustomException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.dtos.MeasurePerHourDTO;
import ro.tuc.ds2020.dtos.builders.MeasureBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Measure;
import ro.tuc.ds2020.repositories.MeasureRepository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service("measureService")
public class MeasureService {
    @Autowired
    MeasureRepository measureRepository;
    @Autowired
    DeviceService deviceService;
    @Autowired
    private SimpMessagingTemplate template;


    private Measure measure = new Measure();
    public Measure save(Measure m, Long did) throws CustomException {
        Device device = deviceService.findById(did);
        if(device == null){
            throw new ResourceNotFoundException("Device with id "+did+" not found");
        }
        m.setDevice(device);
        return measureRepository.save(m);
    }

    public void save(Measure m) {
        measureRepository.save(m);
    }

    public Measure findById(Long id) {
        return measureRepository.findById(id).get();
    }

    public Measure saveEdit(Measure measure) {
        Measure m = measureRepository.findById(measure.getMeasureId()).get();
        //do edits
        return measureRepository.save(m);
    }

    @Transactional
    public void delete(Long id) {

        measureRepository.deleteById(id);
    }
    public List<Measure> listAllByDeviceId(Long id) {
        return measureRepository.findAllByDevice_DeviceId(id);
    }

    public List<Measure> listAllByUserId(Long id) {
        return measureRepository.findAllByDevice_User_UserId(id);
    }

    public List<Measure> getMeasuresForDevice(String deviceName) {
        return this.measureRepository.findAllByDevice_DeviceName(deviceName);
    }

    public List<Measure> listAllByDeviceIdAndDate(Long id, Timestamp date) {
        ZonedDateTime zonedDateTime = date.toInstant().atZone(ZoneId.of("UTC"));
        Timestamp newDate = Timestamp.from(zonedDateTime.plus(1, ChronoUnit.DAYS).toInstant());
        return this.listAllByDeviceId(id).stream().filter((measure)->{
            return ((measure.getTimestamp().compareTo(date)==1 ||
                    measure.getTimestamp().compareTo(date)==0 ) &&
                    measure.getTimestamp().compareTo(newDate)==-1
            );
        }).collect(Collectors.toList());
    }

    public Map<Timestamp, List<MeasurePerHourDTO>> listAllByUserIdAndNoDays(Long id, int noDays) {
        List<Measure> measures = this.listAllByUserId(id);
        Timestamp date = new Timestamp(System.currentTimeMillis());
        ZonedDateTime zonedDateTime = date.toInstant().atZone(ZoneId.of("UTC"));
        Map<Timestamp, List<MeasurePerHourDTO>> hm = new HashMap<>();
        for(int i=0; i<noDays; i++){
            Timestamp newDate = Timestamp.from(zonedDateTime.minus(i, ChronoUnit.DAYS).toInstant());
            List<MeasurePerHourDTO> l = this.listAllByDate(newDate, measures).stream().map(MeasureBuilder::toMeasurePerHourDTO).collect(Collectors.toList());
            hm.put(newDate, l);
        }
        return hm;
    }

    private List<Measure> listAllByDate(Timestamp date, List<Measure> measures) {
        ZonedDateTime zonedDateTime = date.toInstant().atZone(ZoneId.of("UTC"));
        Timestamp newDate = Timestamp.from(zonedDateTime.plus(1, ChronoUnit.DAYS).toInstant());
        return measures.stream().filter((measure)->{
            return ((measure.getTimestamp().compareTo(date)==1 ||
                    measure.getTimestamp().compareTo(date)==0 ) &&
                    measure.getTimestamp().compareTo(newDate)==-1
            );
        }).collect(Collectors.toList());
    }


    public Map<Integer, List<MeasurePerHourDTO>> getBaselineByUserId(Long id) {
        List<Measure> measures = this.listAllByUserId(id);
        Timestamp date = new Timestamp(System.currentTimeMillis());
        ZonedDateTime zonedDateTime = date.toInstant().atZone(ZoneId.of("UTC"));
        Map<Integer, List<MeasurePerHourDTO>> hm = new HashMap<>();
        List<MeasurePerHourDTO> listMeasures = new ArrayList<>();
        for(int i=0; i<7; i++){
            Timestamp newDate = Timestamp.from(zonedDateTime.minus(i, ChronoUnit.DAYS).toInstant());
            listMeasures.addAll(this.listAllByDate(newDate, measures).stream().map(MeasureBuilder::toMeasurePerHourDTO).collect(Collectors.toList()));
        }

        for(int i=0; i<24; i++){
            int finalI = i;
            List<MeasurePerHourDTO> l = listMeasures.stream().filter(measure -> measure.getHour()== finalI)
                    .collect(Collectors.toList());
            hm.put(finalI,l);
        }
        return hm;
    }

    public List<String> getStartTimes(Long id, Integer duration){
        Map<Integer, List<MeasurePerHourDTO>> hm = this.getBaselineByUserId(id);
        Map<String, Integer> intervalSumMap = new HashMap<>();
        List<Double> baselineMeasures = hm.values().stream()
                .map(list->{return list.stream()
                    .mapToDouble(d -> d.getEnCons())
                    .average()
                    .orElse(0.0);})
                .collect(Collectors.toList());
        for(int i = 0; i<baselineMeasures.size()-duration; i++){
            int sum = 0;
            for(int j = 0; j < duration; j++){
                sum += baselineMeasures.get(i+j);
            }
            intervalSumMap.put(""+i+"-"+(i+duration), sum);
        }

         List<String> sortedIntervals = intervalSumMap.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .map(Map.Entry::getKey)
                            .collect(Collectors.toList());
        return sortedIntervals;
    }


    HashMap<Long, MeasureDTO> mapDeviceMeasure = new HashMap<>();

    public void saveMeasure(MeasureDTO measureDTO) {
        Device device = deviceService.findById(measureDTO.getDeviceId());
        if(device != null){
            savePeakValue(device, measureDTO);
        }
    }

    /*
        1 min = 60.000 milliseconds
        1 hour = 60 * 60.000 milliseconds = 3 600 000 milliseconds
     */
    private void savePeakValue(Device device,MeasureDTO measureDTO){

        if(mapDeviceMeasure.containsKey(measureDTO.getDeviceId())){
            MeasureDTO lastMeasure = mapDeviceMeasure.get(measureDTO.getDeviceId());
            double Kw = (lastMeasure.getEnCons() - measureDTO.getEnCons()) / ( (lastMeasure.getTimestamp().getTime() - measureDTO.getTimestamp().getTime()) /  3_600_000.0);
            System.out.println("Here5: " + Kw);
            if (Kw > device.getSensorMaxVal()){
                System.out.println("sending now and hope it works");
                this.template.convertAndSend("/topic/greetings", new WebSocketPayload(device.getUser().getUserId(), "Warning! Peak value from on device "+device.getDeviceName()+" exceeds the maximum value of "+device.getSensorMaxVal()));
            }

            Measure newMeasure = new Measure(measureDTO.getTimestamp(), Kw);
            save(newMeasure, device.getDeviceId());
        }
        mapDeviceMeasure.put(measureDTO.getDeviceId(), measureDTO);

    }
}
