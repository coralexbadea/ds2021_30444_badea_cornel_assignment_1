package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.CustomException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ParameterValidationException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DeviceService {
    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    UserRepository userRepository;

    public List<Device> findByMname(String dname){ return deviceRepository.findByDeviceName(dname);}

    public List<Device> listAll() {
        return deviceRepository.findAll();
    }

    public Device save(Device device) {
        return deviceRepository.save(device);
    }

    public Device save(Device device, Long userId) throws CustomException {
        User user = userRepository.findById(userId).get();
        if(user == null){
            throw new ResourceNotFoundException("User with id "+userId+" not found");
        }
        if(device.getSensorMaxVal() < device.getMaxEnCons()){
            throw new ParameterValidationException("sensor max value", Arrays.asList("less than device max consumption"));
        }
        device.setUser(user);
        return deviceRepository.save(device);
    }


    public Device findById(Long id) {
        return deviceRepository.findById(id).get();
    }

    public Device saveEdit(Device device) {
        return deviceRepository.save(device);
    }

    public Device saveEdit(Long id, Device device) throws CustomException {
        Device deviceDB = this.findById(id);
        if(deviceDB == null){
            throw new ResourceNotFoundException("Device "+id+" not found");
        }
        deviceDB.setDeviceName(device.getDeviceName());
        deviceDB.setLocation(device.getLocation());
        deviceDB.setMaxEnCons(device.getMaxEnCons());
        deviceDB.setSensorDescription(device.getSensorDescription());
        deviceDB.setSensorMaxVal(device.getSensorMaxVal());
        return deviceRepository.save(deviceDB);
    }

    public void delete(Long id) throws CustomException {
        Device device = deviceRepository.findById(id).get();
        if (device == null){
            throw new ResourceNotFoundException("Device "+id+" not found");
        }
        deviceRepository.delete(device);
    }

    public List<Device> getDevicesForUser(Long uid) {
        return this.deviceRepository.findByUser_UserId(uid);
    }
}
