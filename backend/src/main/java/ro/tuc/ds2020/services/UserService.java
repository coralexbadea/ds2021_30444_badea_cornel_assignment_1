package ro.tuc.ds2020.services;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.DuplicateResourceException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.RoleRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service("userService")
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User findUserByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    public Long saveUser(User user) {
        if(checkDuplicate(user.getUserName())){
            throw new DuplicateResourceException("Username already exists");
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setUserEnabled(true);
        Role userRole = roleRepository.findByRoleName("CLIENT");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        return userRepository.save(user).getUserId();
    }

    public Long saveUser(Long id, User user) {
        User user1 = this.userRepository.findById(id).get();
        if(user1 == null){
            throw new ResourceNotFoundException("User" + id+" not found");
        }

        if(user1.getUserName().compareTo(user.getUserName()) != 0){
            if(checkDuplicate(user.getUserName())){
                throw new DuplicateResourceException("Username already exists");
            }
        }

        user1.setName(user.getName());
        user1.setUserName(user.getUserName());
        user1.setAddress(user.getAddress());
        user1.setBdate(user.getBdate());

        return userRepository.save(user1).getUserId();
    }



    @Transactional
    public List<User> listAllByRole(String role ) {

        return userRepository.findAllByRoles_RoleName(role);
    }

    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return findUserByUserName(auth.getName());

    }

    public boolean checkDuplicate(String userName){
        User user = this.userRepository.findByUserName(userName);
        if(user != null){
            return true;
        }
        return false;
    }

}
