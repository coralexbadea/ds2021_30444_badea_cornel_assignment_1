package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContextUtils;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.CustomException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceReadDTO;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.dtos.MeasurePerHourDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.MeasureBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Measure;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.MeasureService;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/measure")
public class MeasureController {

    @Autowired
    private MeasureService measureService;

    @GetMapping("/forDevice/{id}")
    public ResponseEntity<List<MeasureDTO>> getMeasure(@PathVariable Long id){
        List<MeasureDTO> measureDTOList = measureService.listAllByDeviceId(id).stream()
                .map(MeasureBuilder::toMeasureDTO).collect(Collectors.toList());
        return new ResponseEntity<>(measureDTOList, HttpStatus.OK);
    }

    @PostMapping("/forDeviceOneDay/{id}")
    public ResponseEntity<List<MeasurePerHourDTO>> getMeasuresFromDate(@PathVariable Long id, @RequestBody MeasureDTO payload){

        System.out.println(new Date(payload.getTimestamp().getTime()));
        List<MeasurePerHourDTO> measureDTOList = measureService.listAllByDeviceIdAndDate(id, payload.getTimestamp()).stream()
                .map(MeasureBuilder::toMeasurePerHourDTO).collect(Collectors.toList());
        return new ResponseEntity<>(measureDTOList, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<MeasureDTO> postMeasure(@RequestBody MeasureDTO measureDTO){

        Measure measure = MeasureBuilder.toEntity(measureDTO);
        try{
            Measure measureAdded = measureService.save(measure, measureDTO.getDeviceId());
            return new ResponseEntity<>(MeasureBuilder.toMeasureDTO(measureAdded), HttpStatus.OK);
        }
        catch (CustomException exception){
            throw exception;
        }
    }


}
