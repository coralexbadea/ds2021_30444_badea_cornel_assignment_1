package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class IndexController {

    @GetMapping(value = "/test")
    public ResponseEntity<String> getStatus() {
        Authentication authentication1 = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication1 instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication1.getName();
            System.out.println("asdf");
            System.out.println(currentUserName);
            System.out.println("asdfasdf");
        }
        return new ResponseEntity<>("City APP Service is running...", HttpStatus.OK);
    }
}
