package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.CustomException;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserLoginDTO;
import ro.tuc.ds2020.dtos.UserRegisterDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.security.TokenProvider;
import ro.tuc.ds2020.services.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    UserService userService;


    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;


    @PostMapping("/register")
    public ResponseEntity<Long> Register(@Valid @RequestBody UserLoginDTO userRegisterDTO) {
        Long userId = userService.saveUser(UserBuilder.fromLoginToEntity(userRegisterDTO));
        return new ResponseEntity<>(userId, HttpStatus.CREATED);
    }

    @RequestMapping("/giveUser")
    public  ResponseEntity<UserLoginDTO> welcome() {
        User user = userService.getCurrentUser();
        System.out.println(user.getName());
        UserLoginDTO userLoginDTO = UserBuilder.toLoginDTO(user);
        return new ResponseEntity<>(userLoginDTO, HttpStatus.OK);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> generateToken(@RequestBody UserRegisterDTO loginUser) throws AuthenticationException {
        System.out.println("Here111");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUserName(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);

        User user = userService.getCurrentUser();
        System.out.println(user.getName());
        UserLoginDTO userLoginDTO = UserBuilder.toLoginDTO(user);
        userLoginDTO.setToken(token);
        return new ResponseEntity<>(userLoginDTO, HttpStatus.OK);

    }

    @GetMapping("/login")
    public ResponseEntity<String> Register() {
        throw new ResourceNotFoundException("login");
//        return new ResponseEntity<>("GET_LOGIN", HttpStatus.BAD_REQUEST);
    }



    @GetMapping("/user")
    public ResponseEntity<List<UserLoginDTO>> getUsers(){
        List<UserLoginDTO> userDTOList = userService.listAllByRole("CLIENT").stream()
                .map(UserBuilder::toLoginDTO).collect(Collectors.toList());
        return new ResponseEntity<>(userDTOList, HttpStatus.OK);
    }


    @DeleteMapping("/user/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id){
        try{
            userService.delete(id);
            return new ResponseEntity<>("User "+id+" deleted", HttpStatus.OK);
        }
        catch(CustomException exception){
            throw exception;
        }
    }

    @PutMapping("/user/{id}")
    public ResponseEntity putUser(@RequestBody UserLoginDTO userLoginDTO, @PathVariable Long id){
        User user = UserBuilder.fromLoginToEntity(userLoginDTO);
        try{
            userService.saveUser(id, user);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (CustomException exception){
            throw exception;
        }
    }


}
