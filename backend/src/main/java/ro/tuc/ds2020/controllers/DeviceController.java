package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.CustomException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceReadDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.services.DeviceService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @GetMapping("/forUser/{uid}")
    public ResponseEntity<List<DeviceDTO>> getDevices(@PathVariable Long uid){
        List<DeviceDTO> deviceDTOList = deviceService.getDevicesForUser(uid).stream()
                .map(DeviceBuilder::toDeviceDTO).collect(Collectors.toList());
        return new ResponseEntity<>(deviceDTOList, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<DeviceDTO> postDevice(@RequestBody DeviceReadDTO deviceDTO){
        Device device = DeviceBuilder.toEntity(deviceDTO);
        try{
            Device deviceAdded = deviceService.save(device, deviceDTO.getUserId());
            return new ResponseEntity<>(DeviceBuilder.toDeviceDTO(deviceAdded), HttpStatus.OK);
        }
        catch (CustomException exception){
            throw exception;
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<DeviceDTO> putDevice(@RequestBody DeviceReadDTO deviceReadDTO, @PathVariable Long id){
        Device device = DeviceBuilder.toEntity(deviceReadDTO);
        try{
            Device updatedDevice = deviceService.saveEdit(id, device);
            return new ResponseEntity<>(DeviceBuilder.toDeviceDTO(updatedDevice), HttpStatus.OK);

        }
        catch (CustomException exception){
            throw exception;
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteDevice(@PathVariable Long id){
        try{
            deviceService.delete(id);
            return new ResponseEntity<>("Device "+id+" deleted", HttpStatus.OK);
        }
        catch(CustomException exception){
            throw exception;
        }
    }
}

