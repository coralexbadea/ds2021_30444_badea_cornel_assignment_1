package ro.tuc.ds2020.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUserName(String userName);

    List<User> findAllByRoles_RoleName(String role);

}

