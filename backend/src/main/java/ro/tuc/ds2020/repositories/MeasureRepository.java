package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Measure;

import java.util.List;

@Repository
public interface MeasureRepository extends JpaRepository<Measure, Long> {
    List<Measure> findAllByDevice_User_UserId(Long id);
    List<Measure> findAllByDevice_DeviceId(Long id);

    List<Measure> findAllByDevice_DeviceName(String deviceName);
}
