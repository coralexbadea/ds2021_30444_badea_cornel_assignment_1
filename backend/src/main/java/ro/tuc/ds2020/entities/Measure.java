package ro.tuc.ds2020.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;


@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
public class Measure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long measureId;

    private Timestamp timestamp;
    private double enCons;
    @ManyToOne
    @JoinColumn(name="deviceId")
    private Device device;

    public Measure(Timestamp timestamp, double enCons) {
        this.timestamp=timestamp;
        this.enCons=enCons;
    }

    public Measure(Long measureId, Timestamp timestamp, double enCons) {
        this.measureId=measureId;
        this.timestamp=timestamp;
        this.enCons=enCons;
    }
}
