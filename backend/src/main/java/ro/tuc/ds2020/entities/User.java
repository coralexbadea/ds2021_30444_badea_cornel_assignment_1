package ro.tuc.ds2020.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
   // @NotNull(message = "*Please provide a password")
    private String password;
    @Column(name = "userEnabled")
    private Boolean userEnabled;
    @Column(name = "userName")
    //@Size(min = 5, max=100, message = "*Your user name must have at least 5 characters")
    //@NotNull(message = "*Please provide a user name")
    private String userName;


    @Column(name = "name")
    private String name;
    @Column(name = "bdate")
    private Date bdate;
    @Column(name = "address")
    private String address;



    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="users_roles", joinColumns = @JoinColumn (name="userId"),inverseJoinColumns = @JoinColumn(name="roleId"))
    private Set<Role> roles = new HashSet<>();

    @OneToMany( cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "user")
    private Set<Device> devices = new HashSet<>();


    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public User(String userName, String password, String name, Date bdate, String address) {
        this.userName = userName;
        this.password = password;
        this.name = name;
        this.bdate = bdate;
        this.address = address;
    }

    public User(Long userId, String userName, String password, String name, Date bdate, String address) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.name = name;
        this.bdate = bdate;
        this.address = address;
    }
}
