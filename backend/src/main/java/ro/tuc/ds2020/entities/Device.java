package ro.tuc.ds2020.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceId;
    private String deviceName;
    private String location;
    private double maxEnCons;
    private double avgEnCons;

    private String sensorDescription;
    private double sensorMaxVal;

    @OneToMany( cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "device")
    private Set<Measure> measures = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="userId")
    private User user;

    public Device(String deviceName, String location, double maxEnCons, String sensorDescription, double sensorMaxVal) {
        this.deviceName = deviceName;
        this.location = location;
        this.maxEnCons = maxEnCons;

        this.sensorDescription = sensorDescription;
        this.sensorMaxVal = sensorMaxVal;
    }

}
