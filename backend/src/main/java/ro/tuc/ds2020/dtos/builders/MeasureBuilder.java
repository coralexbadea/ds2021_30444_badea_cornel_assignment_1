package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.dtos.MeasurePerHourDTO;
import ro.tuc.ds2020.entities.Measure;

import java.util.Calendar;
import java.util.GregorianCalendar;

@NoArgsConstructor
public class MeasureBuilder {

    public static MeasureDTO toMeasureDTO(Measure measure){
        return new MeasureDTO(
          measure.getMeasureId(),
          measure.getTimestamp(),
          measure.getEnCons(),
          measure.getDevice().getDeviceId()
        );
    }

    public static MeasurePerHourDTO toMeasurePerHourDTO(Measure measure){
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(measure.getTimestamp());
        return new MeasurePerHourDTO(
                measure.getMeasureId(),
                calendar.get(Calendar.HOUR_OF_DAY),
                measure.getEnCons(),
                measure.getDevice().getDeviceId()
        );
    }



    public static Measure toEntity(MeasureDTO measureDTO){
        return new Measure(
            measureDTO.getMeasureId(),
            measureDTO.getTimestamp(),
            measureDTO.getEnCons()
        );
    }
}
