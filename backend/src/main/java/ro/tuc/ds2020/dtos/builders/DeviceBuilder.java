package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceReadDTO;
import ro.tuc.ds2020.entities.Device;

@NoArgsConstructor
public class DeviceBuilder {


    public static DeviceDTO toDeviceDTO(Device device) {
        return new DeviceDTO(
                device.getDeviceId(),
                device.getDeviceName(),
                device.getLocation(),
                device.getMaxEnCons(),
                device.getAvgEnCons(),
                device.getSensorDescription(),
                device.getSensorMaxVal());
    }

    public static Device toEntity(DeviceReadDTO deviceDTO){
        return new Device(

                deviceDTO.getDeviceName(),
                deviceDTO.getLocation(),
                deviceDTO.getMaxEnCons(),

                deviceDTO.getSensorDescription(),
                deviceDTO.getSensorMaxVal()
        );
    }
}
