package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.UserLoginDTO;
import ro.tuc.ds2020.dtos.UserRegisterDTO;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;

import java.util.stream.Collectors;

@NoArgsConstructor
public class UserBuilder {
    public static User fromRegisterToEntity(UserRegisterDTO userRegisterDTO) {
        return new User(userRegisterDTO.getUserName(),
                userRegisterDTO.getPassword(),
                userRegisterDTO.getName(),
                userRegisterDTO.getBdate(),
                userRegisterDTO.getAddress());
    }

    public static User fromLoginToEntity(UserLoginDTO userLoginDTO) {
        return new User(
                userLoginDTO.getUserId(),
                userLoginDTO.getUserName(),
                userLoginDTO.getPassword(),
                userLoginDTO.getName(),
                userLoginDTO.getBdate(),
                userLoginDTO.getAddress());
    }

    public static UserLoginDTO toLoginDTO(User user) {
        return new UserLoginDTO(
                user.getUserId(),
                user.getUserName(),
                user.getName(),
                user.getAddress(),
                user.getBdate(),
                user.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList())
        );
    }
}
