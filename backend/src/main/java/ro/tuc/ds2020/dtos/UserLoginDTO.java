package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginDTO {
    private Long userId;
    private String userName;
    private String password;
    private String name;
    private Date bdate;
    private String address;
    private List<String> roles;
    private String token;


    public UserLoginDTO(Long userId, String userName,String name, String address, Date bdate, List<String> roles) {
        this.userId = userId;
        this.userName = userName;
        this.name = name;
        this.address = address;
        this.bdate = bdate;
        this.roles = roles;
    }
}
