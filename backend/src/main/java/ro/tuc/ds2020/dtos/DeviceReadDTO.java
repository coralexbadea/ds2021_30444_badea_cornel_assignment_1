package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeviceReadDTO {
    private String deviceName;
    private String location;
    private double maxEnCons;

    private String sensorDescription;
    private double sensorMaxVal;
    private Long userId;
}

