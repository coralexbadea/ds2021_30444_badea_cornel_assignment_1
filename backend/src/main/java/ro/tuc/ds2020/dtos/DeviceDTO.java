package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeviceDTO {
    private Long deviceId;
    private String deviceName;
    private String location;
    private double maxEnCons;
    private double avgEnCons;

    private String sensorDescription;
    private double sensorMaxVal;

}
