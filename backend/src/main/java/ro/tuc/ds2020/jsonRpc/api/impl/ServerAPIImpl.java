package ro.tuc.ds2020.jsonRpc.api.impl;


import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.dtos.MeasurePerHourDTO;
import ro.tuc.ds2020.dtos.builders.MeasureBuilder;
import ro.tuc.ds2020.jsonRpc.api.ServerAPI;
import ro.tuc.ds2020.services.MeasureService;

@Service
@AutoJsonRpcServiceImpl
public class ServerAPIImpl implements ServerAPI {

    @Override
    public long multiplier(long a, long b) {
        return a * b;
    }


    @Autowired
    MeasureService measureService;

    @Override
    public ResponseEntity<List<MeasurePerHourDTO>> rpcGetMeasuresFromDate(long id, MeasureDTO payload) {
        System.out.println(new Date(payload.getTimestamp().getTime()));
        List<MeasurePerHourDTO> measureDTOList = measureService.listAllByDeviceIdAndDate(id, payload.getTimestamp()).stream()
                .map(MeasureBuilder::toMeasurePerHourDTO).collect(Collectors.toList());
        return new ResponseEntity<>(measureDTOList, HttpStatus.OK);
    }

    @Override
    public Map<Timestamp, List<MeasurePerHourDTO>> rpcGetMeasuresLastDays(long id, int d) {
        Map<Timestamp, List<MeasurePerHourDTO>>  hm = measureService.listAllByUserIdAndNoDays(id, d);
        return hm;
    }


    @Override
    public Map<Integer, List<MeasurePerHourDTO>> rpcGetBaseline(long id) {
        Map<Integer, List<MeasurePerHourDTO>>  hm = measureService.getBaselineByUserId(id);
        return hm;
    }

    @Override
    public List<String> rpcGetStartTimes(long id, int duration) {
        return measureService.getStartTimes(id, duration);
    }





}


