package ro.tuc.ds2020.jsonRpc.api;
import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.dtos.MeasurePerHourDTO;
import ro.tuc.ds2020.dtos.builders.MeasureBuilder;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@JsonRpcService("/api")
public interface ServerAPI {

    long multiplier(@JsonRpcParam(value = "a") long a, @JsonRpcParam(value = "b") long b);

    ResponseEntity<List<MeasurePerHourDTO>> rpcGetMeasuresFromDate(@JsonRpcParam(value = "id") long id, @JsonRpcParam(value = "payload") MeasureDTO payload);

    Map<Timestamp, List<MeasurePerHourDTO>>  rpcGetMeasuresLastDays(@JsonRpcParam(value = "id") long id, @JsonRpcParam(value = "d") int d);

    Map<Integer, List<MeasurePerHourDTO>> rpcGetBaseline(@JsonRpcParam(value = "id") long id);

    List<String> rpcGetStartTimes(@JsonRpcParam(value = "id") long id, @JsonRpcParam(value = "duration") int duration);
}

