package ro.tuc.ds2020.rabbitmq;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.MeasureDTO;
import ro.tuc.ds2020.services.MeasureService;


@Component
public class MeasuresConsumer {

    @Autowired
    public MeasureService measureService;


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = MessagingConfig.QUEUE, durable = "true"),
            exchange = @Exchange(value = MessagingConfig.EXCHANGE, ignoreDeclarationExceptions = "true", type = "topic"),
            key = MessagingConfig.ROUTING_KEY)
    )
    public void consumeMessageFromQueue(MeasureDTO measureDTO){
        System.out.println("Here we received a message: " + measureDTO.getEnCons() +"       "+  measureDTO.getDeviceId()+ "    "+ measureDTO.getTimestamp());
        measureService.saveMeasure(measureDTO);
    }

}
