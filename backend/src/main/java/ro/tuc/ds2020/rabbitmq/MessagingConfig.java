package ro.tuc.ds2020.rabbitmq;


import org.hibernate.boot.model.Caching;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {

    public static final String QUEUE = "ds_queue";
    public static final String EXCHANGE = "ds_exchange";
    public static final String ROUTING_KEY = "ds_routingKey";

    @Bean
    public Queue queue() {

        Queue queue = new Queue(QUEUE);
        System.out.println(queue.isDurable());
        return queue;
    }

    @Bean
    public TopicExchange exchange() {
        TopicExchange topic = new TopicExchange(EXCHANGE);
        System.out.println(topic.isDurable());
        return topic;
    }

    @Bean
    public Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY);
    }

    @Bean
    public ConnectionFactory connectionFactory(){
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setUri("amqps://lvhgzwgh:MVRORfooiFDo5jF3EhWRTLa8MWcwwp7g@cow.rmq2.cloudamqp.com/lvhgzwgh");
        return connectionFactory;
    }
    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}
