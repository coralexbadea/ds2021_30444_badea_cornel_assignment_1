package ro.tuc.ds2020.controllers;

import ro.tuc.ds2020.Ds2020TestConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.dtos.UserRegisterDTO;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UserControllerUnitTest extends Ds2020TestConfig  {
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void insertUserTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserRegisterDTO personDTO = new UserRegisterDTO("alexBadea", "scoala3", "Alex Badea", new Date(),"The address where I live");
        mockMvc.perform(post("/api/register")
                .content(objectMapper.writeValueAsString(personDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }
}